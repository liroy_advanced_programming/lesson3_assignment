#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

void clearTable();
void printTable();
int callback(void* notUsed, int argc, char** argv, char** azCol);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

std::unordered_map<std::string, std::vector<std::string>> results;

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)  //Checks if everything is OK
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");  //Cleans the screen
	
	flag = carPurchase(12, 6, db, zErrMsg); //A successful deal
	if (flag) //Checks if everything is Ok
	{
		std::cout << 1 << std::endl;
	}
	
	flag = carPurchase(1, 16, db, zErrMsg); //A successful deal
	if (flag) //Checks if everything is Ok
	{
		std::cout << 1 << std::endl;
	}
	flag = carPurchase(10, 1, db, zErrMsg); //A not successful deal
	if (!flag) //Checks if everything is Ok
	{
		std::cout << 1 << std::endl;
	}
	
	flag = balanceTransfer(1, 2, 3000, db, zErrMsg); //A successful deal
	if (flag) //Checks if everything is Ok
	{
		std::cout << 1 << std::endl;
	}

	flag = balanceTransfer(7, 5, 800000, db, zErrMsg); //A successful deal
	if (!flag) //Checks if everything is Ok
	{
		std::cout << 1 << std::endl;
	}


	system("Pause");
	return 0;
}
//The function resets the table in the data base
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}
//The function prints the table in the data base
void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				std::cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		std::cout << std::endl;
	}
}
//The function gets the returned data and inserts it into the results map
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i = 0;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			std::pair<std::string, std::vector<std::string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}
/*
The function checks if a certain buyer can buy a certain car - if so the deal is done
And the function returns TRUE if not the function returns FALSE
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc = 0;
	int carPrice = 0;
	int buyerBalance = 0;
	std::string query = "";

	//Updates the results map
	clearTable();

	rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) // Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	if (std::stoi(results["available"][carid - 1]) != 1) //Checks if the car is available
	{
		return false;
	}

	carPrice = std::stoi(results["price"][carid - 1]); //Gets the price of the car

	//Updates the results map
	clearTable();

	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) // Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	buyerBalance = std::stoi(results["balance"][buyerid - 1]); //Gets the balance of the buyer

	if (carPrice > buyerBalance)  //Checks if the buyer has enough money to buy the car
	{
		return false;
	}


	//Updates the balance of the buyer
	buyerBalance = buyerBalance - carPrice; 
	query = "update accounts set balance = " + std::to_string(buyerBalance) + " where buyer_id = " + std::to_string(buyerid);
	rc = sqlite3_exec(db, query.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK) // Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}


	//Updates that the car isn't available anymore
	query = "update cars set available = 0 where id = " + std::to_string(carid);
	rc = sqlite3_exec(db, query.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK) // Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	return true;
}
/*
The function transfers money from one account to another
If everything goes right the function returns true - if not reutrns false
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc = 0;
	int moneyFrom = 0;
	int moneyTo = 0;
	std::string query = "";

	//Updates the results map
	clearTable();

	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) // Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	if (std::stoi(results["balance"][from - 1]) < amount)  //Checks if the account has enough money
	{
		return false;
	}

	//Updates the money in the first account
	moneyFrom = std::stoi(results["balance"][from - 1]) - amount;
	query = "update accounts set balance = " + std::to_string(moneyFrom) + " where id = " + std::to_string(from);
	rc = sqlite3_exec(db, query.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK) // Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	//Updates the money in the first account
	moneyTo = std::stoi(results["balance"][to - 1]) + amount;
	query = "update accounts set balance = " + std::to_string(moneyTo) + " where id = " + std::to_string(to);
	rc = sqlite3_exec(db, query.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK) // Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	return true;
}