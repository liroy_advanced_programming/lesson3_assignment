#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

void clearTable();
void printTable();
int callback(void* notUsed, int argc, char** argv, char** azCol);

std::unordered_map<std::string, std::vector<std::string>> results;

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)  //Checks if everything is OK
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");  //Cleans the screen

	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name string)", NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK) //Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	rc = sqlite3_exec(db, "insert into people(name) values('Liroy')", NULL, 0, &zErrMsg); //Inserts name 1
	if (rc != SQLITE_OK)  //Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}


	rc = sqlite3_exec(db, "insert into people(name) values('Elihai')", NULL, 0, &zErrMsg);  //Inserts name 2
	if (rc != SQLITE_OK)  //Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	

	rc = sqlite3_exec(db, "insert into people(name) values('NoName')", NULL, 0, &zErrMsg); //Inserts name 3
	if (rc != SQLITE_OK)  //Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	rc = sqlite3_exec(db, "update people set name='Eilon' where id=3", NULL, 0, &zErrMsg); //Updates name 3
	if (rc != SQLITE_OK)  //Checks if everything is OK
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}


	//Printing the table
	clearTable();
	rc = sqlite3_exec(db, "select * from people", callback, 0, &zErrMsg); 

	if (rc != SQLITE_OK)  
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	return 0;
}
//The function resets the table in the data base
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}
//The function prints the table in the data base
void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				std::cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		std::cout << std::endl;
	}
}
//The function gets the returned data and inserts it into the results map
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i = 0;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			std::pair<std::string, std::vector<std::string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}